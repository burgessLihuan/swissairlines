import '../../modules/helper-modules/base';
import './style.scss';
import 'slick-carousel/slick/slick.js';
import 'slick-carousel/slick/slick.scss';
import 'slick-carousel/slick/slick-theme.scss';

var slider =$("[data-item='bg']").slick({
    autoplay: true,
    autoplaySpeed: 3000,
});
$('button').click(function () {
    slider.slick("slickPause");
});
$("[data-item='aircraft-tips']").click()

$("[data-click='postcard']").on('click',function (e) {
    var submit_arry={};
    var url = $(e.currentTarget).data("url");
    submit_arry.index = $("[aria-hidden='false']").data("slickIndex");
    if(url){
        window.location.href = url + $.param(submit_arry);
    }
});