import '../../modules/helper-modules/base';
import './style.scss';
import resLoader from "./resLoader.js"
window.resLoader = resLoader;

function loadingStarting() {
    var loader = new resLoader({
        resources : window.loadingData.imgs,
        onStart : function(total){
            console.log('start:'+total);
        },
        onProgress : function(current, total){
            console.log(current+'/'+total);

        },
        onComplete : function(total){
            console.log(total);
             location.href = window.loadingData.url;
        }
    });

    $( document ).ready(function() {
        loader.start();
    });

}
loadingStarting();
