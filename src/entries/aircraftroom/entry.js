import '../../modules/helper-modules/base';
import './style.scss';
var tips = $("[data-item='aircraft-tips']");


$("[data-click='aircraft-food']").on('click',function () {
    tips.show();
    $("[data-item='food']").show();
});
$("[data-click='aircraft-entertainment']").on('click',function () {
    tips.show();
    $("[data-item='entertainment']").show();
});
$("[data-click='aircraft-chair']").on('click',function () {
    tips.show();
    $("[data-item='chair']").show();
});
$("[data-click='service-lose']").on('click',function () {
    tips.hide();
    tips.children().each(function (item) {
        $(this).hide();
    });
});